! Copyright (C) 2019 - Juan Pabo Carbajal
!
! This file is free software; you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation; either version 2 of the License, or
! (at your option) any later version.
!
! This file is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this file. If not, see <http://www.gnu.org/licenses/>.

subroutine string_fortran (caps, string)

  implicit none

  character(len = 7), intent(in out) :: string 
  logical, intent(in) :: caps

  string = 'fortran'
  if (caps) call To_upper(string)
  return

end subroutine string_fortran

subroutine To_upper(str)

  character(*), intent(in out) :: str
  integer :: i

  do i = 1, len(str)
    select case(str(i:i))
      case("a":"z")
        str(i:i) = achar(iachar(str(i:i))-32)
    end select
  end do 

end subroutine To_upper
