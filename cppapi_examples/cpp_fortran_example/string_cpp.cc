/*
 * Copyright (C) 2017 - Juan Pabo Carbajal
 *
 * This file is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this file. If not, see <http://www.gnu.org/licenses/>.
 */

#include <algorithm>
#include <string>

std::string string_cpp (bool cap)
{
  std::string cpp = "C++";
  if (!cap)
    std::transform(cpp.begin(), cpp.end(), cpp.begin(), ::tolower);
  return cpp;
}
