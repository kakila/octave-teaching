## Copyright (C) 2019 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

function printlang (lang, capitalize=true)

  printf ("Language: ");
  switch lang
    case 'f'
      autoload ('__string_fortran__', '__printlang__.oct')
      printf ("%s\n", __string_fortran__(capitalize));
      autoload ('__string_fortran__', '__printlang__.oct', 'remove')
    case 'cpp'
      autoload ('__string_cpp__', '__printlang__.oct')
      printf ("%s\n", __string_cpp__(capitalize));
      autoload ('__string_cpp__', '__printlang__.oct', 'remove')
  endswitch

endfunction

%!demo
%! # All upper
%! printlang('cpp');
%! printlang('f');
%! # All lower
%! printlang('cpp', false);
%! printlang('f', false);

