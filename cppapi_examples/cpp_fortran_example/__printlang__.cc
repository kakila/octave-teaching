/*
 * Copyright (C) 2019 - Juan Pabo Carbajal
 *
 * This file is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this file. If not, see <http://www.gnu.org/licenses/>.
 */

#include <octave/oct.h>
#include <octave/f77-fcn.h>

#include <string>

extern "C"
{
  F77_RET_T F77_FUNC (string_fortran, STRING_FORTRAN) 
                  (const F77_LOGICAL&, F77_CHAR_ARG_DECL F77_CHAR_ARG_LEN_DECL);
}

DEFUN_DLD (__string_fortran__, args, , "Takes one bool argument, returns 'fortran'")
{
  if (args.length () != 1)
    print_usage ();

  // Input value list
  F77_LOGICAL capitalize = args(0).int_value ();

  // Output containers
  OCTAVE_LOCAL_BUFFER (char, string, 7);

  // Call fortran subroutine
  F77_XFCN (string_fortran, STRING_FORTRAN, 
                                     (capitalize, string F77_CHAR_ARG_LEN (7)));
  // Return value list
  octave_value_list retval;
  retval(0) = std::string (string);
  return retval;
}

extern std::string string_cpp (bool);

DEFUN_DLD (__string_cpp__, args, , "Takes one bool argument, returns 'C++'")
{
  if (args.length () != 1)
    print_usage ();

  // Input value list
  bool capitalize = args(0).bool_value ();
  
  // Output containers
  charMatrix string (3);

  // Call CPP function
  string = string_cpp (capitalize);

  // Return value list
  octave_value_list retval;
  retval(0) = string;
  return retval;
}

