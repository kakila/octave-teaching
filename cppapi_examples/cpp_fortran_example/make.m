## Copyright (C) 2019 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## Compilation of emulators in windows systems
# Run this file to compile the emulators in Windows
# You need to have SUNDIALS installed in your system

CPP_FLAGS = {'-Wall', '-std=c++11', '-O3'};
F90_FLAGS = {'-Wall', '-O3'};

SOURCES = struct (
    'fortran', 'string_fortran', 
    'cpp', 'string_cpp', 
    'oct', '__printlang__');

## FORTRAN
printf ('Compiling FORTRAN\n');
fflush (stdout);
mkoctfile ('-v', F90_FLAGS{:}, ...
           sprintf ('-o %s.o',SOURCES.fortran), ...
           '-c', ...
           sprintf ('%s.f90',SOURCES.fortran));
printf('\n');
fflush (stdout);

## C++
printf ('Compiling C++\n');
fflush (stdout);
[out, st] = mkoctfile ('-v', CPP_FLAGS{:}, ...
           sprintf ('-o %s.o',SOURCES.cpp), ...
           '-c', ...
           sprintf ('%s.cc',SOURCES.cpp));
printf (out);
printf('\n');
fflush (stdout);

printf ('Compiling Octave function\n');
fflush (stdout);

## Octave function
[out, st] = mkoctfile ('-v', CPP_FLAGS{:}, ...
           sprintf ('-o %s.oct',SOURCES.oct), ...
           sprintf ('%s.cc',SOURCES.oct), ...
           sprintf ('%s.o',SOURCES.fortran), ...
           sprintf ('%s.o',SOURCES.cpp)
           );
printf (out);
printf('\n');
fflush (stdout);

clear all

