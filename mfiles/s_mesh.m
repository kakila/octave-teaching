## Copyright (C) 2017 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

clear all
Nx = 100;                     # Number of points in x
x  = linspace (0, 2, Nx);    # 1D Mesh
Ny = 20;                     # Number of points in y
y  = linspace (0, 1, Ny);       # 1D Mesh

## Manually
# 2D mesh
tic
X = Y = zeros (Ny, Nx);
for i=1:Nx
  for j=1:Ny
    X(j,i) = x(i);
    Y(j,i) = y(j);
  endfor
endfor
toc

## repmat
# is quicker
tic
X_ = repmat (x, Ny, 1);
Y_ = repmat (y(:), 1, Nx);
toc
assert (X == X_ & Y == Y_);

## meshgrid
# is optimal
tic
[Xo Yo] = meshgrid (x,y);
toc
assert (X == Xo & Y == Yo);

## Plot the result
figure (1)
clf
mesh (X, Y, zeros (size (X)));
view (2)
axis image
xlabel ('X');
ylabel ('Y');
