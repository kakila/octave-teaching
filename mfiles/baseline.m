## Copyright (C) 2015 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## [Y min max] = baseline (X)
## ... = baseline (X, YMIN)
## Map the columns of X to the interval [0, 1].
## The maximum value of each column maps to 1, the minimum to 0.
## The minimum and max values are returned in min y max, respectively.
##
## If a second input argument YMIN is provided it defines the minimmum value
## of the output Y. YMIN can be larger than 1.
##
## Example:
##    t = 2 * pi * linspace (0,1,100).';
##    x = 5 * sin (t) - 1;
##    y = baseline (x);
##    [x_max x_idx] = max (x);
##    printf ('Max. X=%.2g at index %d\n', x_max, x_idx);
##    printf ('Value of Y(%d)=%.2g\n', x_idx, y(x_idx));
##    [x_min x_idx] = min (x);
##    printf ('Min. X=%.2g at index %d\n', x_min, x_idx);
##    printf ('Value of Y(%d)=%.2g\n', x_idx, y(x_idx));

function [y xm xM] = baseline (x, ymin = 0)
  if (ymin > 1)
    error ('ymin should be lower or equal than 1');
  end

  xm = min (x);
  xM = max (x);

  y  = (x - xm) ./ (xM - xm);
  y  = y * ( 1 - ymin ) + ymin;
end

%% TESTS
%!error baseline ([1;2], 1+eps);

%% DEMO
%!demo
%! t = 2 * pi * linspace (0,1,100).';
%! x = 5 * sin (t) - 1;
%! y = baseline (x);
%! [x_max x_idx] = max (x);
%! printf ('Max. X=%.2g at index %d\n', x_max, x_idx);
%! printf ('Value of Y(%d)=%.2g\n', x_idx, y(x_idx));
%! [x_min x_idx] = min (x);
%! printf ('Min. X=%.2g at index %d\n', x_min, x_idx);
%! printf ('Value of Y(%d)=%.2g\n', x_idx, y(x_idx));
