## Copyright (C) 2017 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

clear all

function x = dummy (n)
  for i=1:n
    x(i) = rand;
  endfor
endfunction

function dummy2 (n)
  x = dummy (n);
  l = length (x);
endfunction

profile on

for n = 10:10:100;
  dummy2 (n);
endfor

profile off

profshow
disp ("\nExplore the function calls, when done write quit");
profexplore

profile clear

disp ("\nWe will study the time complexity of some functions.")
disp ('When ready press Enter');
pause;
close all
speed ('for i=1:n; dummy (n); endfor', '', 500);
speed ('sort (x);', 'x = randn (n,1);', 1e5);
speed ('cholinv (x);', "x = randn (n); x*=x.';", 1.5e3);

