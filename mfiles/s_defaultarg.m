## Copyright (C) 2018 juanpi
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Author: juanpi <juanpi@poma>
## Created: 2018-03-07

1;

function z = add_s (x, y, a=1.0)
  z = x + a * y;
endfunction

x = 1;
y = 2;
printf ('x=%f, y=%f\n',x,y);
printf('Using default: %f\n', add_s(x,y));
a = 3;
printf('Using a=%f: %f\n', a, add_s(x,y,a));
