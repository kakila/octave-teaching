## Copyright (C) 2018 LIPartner AG
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-05-17

## -*- texinfo -*-
## @defun {[@var{Q}, @var{V}] =} circular_pipe_hydraulic (@var{D}, @var{S}, @var{h}, @var{n})
## @defunx {[@dots{}, @var{R_h}, @var{A}, @var{P}, @var{t}] =} circular_pipe_hydraulic (@dots{})
## @defunx {[@dots{}] =} circular_pipe_hydraulic (@dots{}, @var{correction}=false)
## Volumetric flow @var{Q} and speed of flow @var{V} of a partially filled circular
## pipe, using Manning's formulas.
##
## The inputs are the diameter (internal) of the pipe @var{D}, the slope of the 
## energy grade line @var{S}, the water depth @var{h} and the Manning's 
## coefficient of roughness @var{n}.
## Volumetric flow @var{Q} and speed of flow @var{V} are calculated using Manning's formulas:
##
## @group
##  @var{V} = @var{R_h}^(2/3) * sqrt (@var{S}) / @var{n}
##
##  @var{Q} = @var{A} * @var{V}
## @end group
##
## where @var{A} is the cross-sectional area of flow and @var{R_h} is the hydraulic 
## radius defined as the flow area divided by the wetted perimeter.
## @var{R_h} and @var{A} are returned in the 3rd and 4th output argumnts, resp.
## Both equations are defined in SI units.
##
## The 5th output argument @var{P} is the wetted perimeter, i.e. the length of the arc
## in contact with the fluid.
##
## The 6th output argument @var{t} is the fluid surface angle in radians, i.e.
## the angle of the circular arc corresponding to the wetted perimeter.
## 
## If the optional input argument @var{corrected} is set to @command{true}, then
## the wetted perimenter is corrected following the correction proposed by 
## Escritt (1984). The default is value is @command{false}. See reference below.
##
## Reference:
##
## Akgiray, Ömer. (2004). Simple Formulae for Velocity, Depth of Flow, and Slope
## Calculations in Partially Filled Circular Pipes. 
## Environmental Engineering Science 21. 371-385. 
## DOI: 10.1089/109287504323067012.
##
## @end defun

function  [Q V R_h A P theta] = circular_pipe_hydraulic (D, S, h, n, correct_Rh=false)

  % fluid surface angle (full arc angle)
  theta = 2 * acos (1 - 2 * h ./ D); % Ref. eq. (3)
  % wetted area
  A     = ( D.^2 / 8 ) .* (theta - sin (theta)); % Ref. eq. (4)
  % wetted perimeter
  P     = ( D / 2 ) .* theta; % R * theta
  
  if !correct_Rh
    R_h = A ./ P;
  else
    R_h = A ./ ( P + (D / 2) .* sin (theta/2) ); % Ref. eq. (5)
  endif

  % Speed of flow
  V = R_h.^(2/3) .* sqrt (S) / n; % Ref. eq (1)
  % Volumetric flow
  Q = A .* V; % Ref. eq (2)

endfunction

%!demo
%! D = 125e-3; % diameter [m]
%! S = 1e-2; % slope of the energy grade line
%! h = 0.5 * D; % water depth [m]
%! n = 0.9234e-2; % Manning's coeff. Steel
%!
%! [Q V Rh A P t] = circular_pipe_hydraulic (D, S, h, n)
%! printf ('Flow: %g [L/min]\n', Q * 60e3);

%!demo
%! D = 100e-3; % diameter [m]
%! S = 0.1; % slope of the energy grade line
%! h = D / 4; % water depth [m]
%! n = 1.2e-2; % Manning's coeff. Corrugated PE with smooth inner walls
%!
%! [Q V Rh A P t] = circular_pipe_hydraulic (D, S, h, n);
%! [Qe, Ve, Re] = circular_pipe_hydraulic (D, S, h, n, true);
%!
%! printf ("Flow area: %g [m²]\nWetted perimeter: %g [m]\nWater surf. angle: %g [rad]\n", ...
%!    A, P, t);
%! printf ("\n** Default values\n")
%! printf ("Flow: %g [m³/s]\nSpeed: %g [m/s]\nHyd. radius: %g [m]\n", ...
%!    Q, V, Rh);
%! printf ("\n** Corrected values\n")
%! printf ("Flow: %g [m³/s]\nSpeed: %g [m/s]\nHyd. radius: %g [m]\n", ...
%!    Qe, Ve, Re);
