## Copyright (C) 2018 juanpi
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Author: juanpi <juanpi@poma>
## Created: 2018-03-07

x = linspace (0, 1, 1e3);

# This is the foor loop
j_for = 0;
tic
for ( i = x )
  #disp (i);
  j_for = j_for + i;
end %for over i
toc

# This is the while loop
j_while = 0;
i       = x(1);
dx      = x(2) - x(1);
tic
while ( i <= x(end) )
  j_while += i;
  i       += dx;
end % while over i
toc

j_while2 = 0;
i        = x(1);
tic
while ( true )
  j_while2 += i;
  i       += dx;
  
  if (i > x(end))
    break
  end % if
  
end % while over i
toc

tic
xsum = sum (x);
toc

printf ('Result: (for)%.2f, (while)[%.2f, %.2f], (sum)%.2f\n', j_for, ...
                                               j_while, j_while2, xsum);
