## Copyright (C) 2019 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2019-06-02

## -*- texinfo -*-
## @defun copyarg (@var{x})
##  Make an internal copy of @var{x} and show memory usage
## @end defun

function copyarg (x)
  m0 = free_memory;
  sum (x(:));
  printf("%.2f MB\n", m0 - free_memory());
  
  
  x(42) = 0.0;
  printf("%.2f MB\n", m0 - free_memory());
endfunction

function free = free_memory()
  free   = str2double ( ...
                  cell2mat (cell2mat ( ...
                  regexp( ...
                  type('/proc/meminfo').', ...
                  "MemFree:(.*?) kB\n", "tokens")))) / 1024;
endfunction
