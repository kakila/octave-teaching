## Copyright (C) 2017 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## Simple CA
# Define sizes and rule
clear all
nT = 200;
nX = 600;

rule =@(x,y) xor (x,y);

## Manual
# evolution
x                       = false (nT, nX); 
x(1,[1 round(nX/2) nX]) = true;

tic;
for i = 1:nT-1
  for j = 2:nX-1
    x(i+1,j) = rule (x(i, j - 1), x(i, j + 1));
  endfor
endfor
toc;

## Sliced
# evolution
x_      = false (nT, nX); 
x_(1,:) = x(1,:);
x__ = x_;

# Spatial slices
curr = 2:(nX-1);
next = curr + 1;
prev = curr - 1;
tic;
for i = 1:nT-1
  x_(i+1, curr) = rule (x_(i, prev), x_(i, next));
endfor
toc;

# Time index
it = (1:nT-1).';
tic;
x__(it+1, curr) = cell2mat ( ...
      arrayfun (@(i)rule (x_(i, prev), x_(i, next)), it, ...
               'UniformOutput', false)
      );
toc;

## Check
# assert the equaity of the reslts and plot
assert (x == x_);
assert (x == x__);
figure (1)
clf
spy (x)
