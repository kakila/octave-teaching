## Copyright (C) 2018 LIPartner AG
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-05-17

## -*- texinfo -*-
## @defun {@var{Q} =} Volumenstrom (@var{d}, @var{v})
## @defunx {[@var{Q}, @var{F}] =} Volumenstrom (@var{d}, @var{v})
## Berechnet den Volumenstrom eines Rohres mit dem Durchmesser @var{d}, 
## wobei die Flüssigkeit mit der Geschwindigkeit @var{v} strömt.
##
## Die Berechnung ist rein geometrisch: Das Volumen ergibt sich aus der 
## Querschnittsfläche des Rohres multipliziert mit der Verdrängung der 
## Flüssigkeit pro Zeiteinheit, d.h. 
## @group
## Q = Volumen / Zeit = Fläche * Verdrängung / Zeit  = Fläche * v
## @end group
##
## Das zweite Ausgabeargument @var{F} enthält die berechnete Querschnittsfläche.
## @end defun

function [Q, F] = Volumenstrom (d, v)
  F = pi * (d / 2).^2;
  Q = F .* v;
endfunction

%!demo
%! d = linspace (0.5,2.5,30); % Durchmesser [m]
%! v = linspace (1, 25, 30); % Flüssigkeitsgeschwindigkeit [m / s]
%! [D V] = meshgrid (d, v);
%! Q = Volumenstrom (D, V);
%! QLmin = Q * (10 / 1 )^3 * ( 60 / 1);
%!
%! figure (1);
%! surf(D, V, QLmin);
%! axis tight
%! xlabel ('Durchmesser [m]')
%! ylabel ('Geschwindigkeit [m / s]')
%! %zlabel ('Volumenstrom [m³ / s]')
%! zlabel ('Volumenstrom [L / min]')
