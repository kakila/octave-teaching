## Copyright (C) 2017 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

clear all
N = 1e5;
x = randn (N, 1);

# Mean vlaue using a loop
tic;           # initialize cronometer
cum = 0;       # variable to accumulate the sum
for i=1:N
  cum += x(i);
endfor
cum /= N;
dt = toc;
printf ("Mean value (loop): %g\n", cum);
printf ("Elapsed time [s]: %.2e\n", dt);

# Mean vlaue using buil-in function
tic;
cum = mean (x);
dt2 = toc;
printf ("Mean value (mean): %g\n", cum);
printf ("Elapsed time [s]: %.2e\n", dt2);

printf ("Vectorized code is %d times faster than the loop\n", floor (dt / dt2))
