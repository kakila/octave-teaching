## GNU Octave: language basics
#
# This script supports the slides on Octave basics.
# Herein we cover operators, indexing, and flow control blocks.
#
##

# Copyright (C) 2019 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2019-06-02

## The assignment operator
# The operator |=| lets us store data into variables we can manipulate
# and pass as arguments to functions.
# 
x = 1;                        # store a constant into  variable named 'x'
this_is_a_long_name = 3.1415; # long names are OK, without special characters
% bad name = 1
x = y = z = 42;               # multiple assignments

##
# Assignments are not references, they are (clever) copies (CoW).
# Here we also omit the ';' at the end of the command to get the result printed
# to the standard output.
#
a = 10;
b = a;    # b has the value contained by 'a', not a link to 'a'.
b = 5     # here we modify 'b' but 'a' stays the same
a

## Arithmetic operators
# You can use Octave as a (scientific) calculator
#
x = 1;
x + 1
x = x + 1
x += 1
x -= 2

## Logical operators
# You can do binary logic with Octave
#
x = 5
T = x > 3
F = x - 2 <= 2
##
T & T
F & F
T | F

##
# Parenthesis can be used to force the order of operations or disambiguate.
# Code style: separate binary operators (and if clearer parenthesis) with spaces
(3 + 2 * x) / 3^2 + 15 - x
 ( (3 + 2 * x) / 3 )^2 + 15 - x
 
## Basic data types
# Illustration of several data types builtin in Octave.
#
clear
tf = true                 # logical: true == 1 == not empty, false == 0 == empty
c  = char (97)            # ASCII characters
w  = int8 (100)           # 8 bits = 1 byte integers
x  = int64 (100)          # 64 bits = 8 bytes integers
y  = double (100.1)       # IEEE floating point format. DEFAULT
z  = complex (1.5, 42.24) # complex numbers, double x2
whos

## Arrays
# Array are ordered list (sequences) of elements of a given type.
# Here a few examples of array creation.
#
clear
tf = [true, true, true]
tf = [true true true]         # space acts as comma
tf = true (1, 3)              # same as above
##
x  = [1.1, 1.6, int8(1), 42.24]
whos x
##
y = rand (1, 4)               # row vector, 4 elements
y = y.'                       # column vector, via transposition
##
clear
y    = [1; 2; 3]                 # column vector
yint = int8 (y)                  # element-wise casting to int8
whos

## Ranges
# A range is a type that represents an array of contiguous values
#
clear
idx = 1:5              # it is a row, (1:5).' would be a column
idx2 = [1 2 3 4 5]
whos
##
clear
x = 0:0.5:2
y = linspace (0, 2, length (x))
z = [0 0.5 1 1.5 2]
whos

## Matrices
# These are 2 dimensional arrays. The dimensions are usually called row and 
# column.
#
A = [1 2 3; 4 5 6]            # 2x3 matrix
B = ...                       # line continuation
[1 2 3
 4 5 6]                       # same as above, new line acts as semi-colon
C = [[1, 2, 3]; [4 5 6]]      # same as above
##
D = randi (10, 4, 3)          # matrix of random integers
##
M = magic (4)                 # 4x4 magic square
##
Z = zeros (2)                 # same as zeros (2, 2)

## Note on transpose
# The quote (|'|) conjugates a matrix: transpose and complex conjugate (|conj|). 
# To avoid conjugation, use dot-quote (|.'|).
# Using |.'| is a clear statement about the intention: "change shape".
#
A = rand (2, 3);
all ( (A.' == A')(:) )     # for real matrices it is the same, alas slower
##
A = complex (A, A);
all ( (A.' == A')(:) )     # not for complex matrices

## Array indexing
# Getting and setting elements in an array.
#
clear
A = [1 2 3; 4 5 6; 7 8 9].'   # matrices are stored in column-major order
##
A(:)                          # flatten the matrix
##
A(1)                          # == 1, first element
A(9)                          # == 9, last element
A(end)                        # == 9, same as above
A(5)                          # == 5, fifth element
A(2,3)                        # == 6, 1st row, 3rd column. Style: no space
##
A(2,2) = -5                   # set the element at (2,2)
##
# we can remove elements, this in general doesn't conserve the shape
x    = [1 2 2 3]
x(2) = []       # remove 2nd element
##
B = A;
B(5) = []              # this gives a vector, B is not square anymore
B = reshape (B, 4, 2)  # we can re-arrange if it makes sense

## Array indexing: ranges
# We can access several items using ranges.
#
A
A(1:3)                       # elements 1, 2, 3
A(2:3, 1:2)                  # sub-matrix

##
# The colon (|:|) is an implicit range that runs over all elements. 
A(1,:)                        # 1st row
A(:,3)                        # 3rd column
##
[rows, cols] = size (A)       # shape of the matrix
##
A(2,:) = -5                   # set the whole 2 row to the same value
A(2,:) = rand(1, cols)        # set the whole 2 row to a vector
A(:,2) = []                   # remove 2nd column

## Array indexing: logical
# We can index with logical conditions; the elements we get coincide with the 
# true values in the logical array. 
# Lets index a 3-by-3 matrix to get the odd index elements.
#
tf = mod (1:9, 2) == 1   # logical row using the modulo function
A  = reshape (1:9, 3, 3) # 3x3 matrix
A(tf)
##
# Lets set all even elements to zero
A(!tf) = 0
##
# We could also use the matrix itself to generate a condition for indexing.
# Lets set all elements within an open interval to zero, and count them
A = rand (5)
in_open_interval = (A > 0.25) & (A < 0.75)
A(in_open_interval) = 0
[sum(in_open_interval(:)) nnz(in_open_interval)]

## Strings
# An array of characters is called a string. You can handle strings just like
# any other array.
#
word = 'bazinga!' # creation
##
double (word)     # ASCII values of each letter
##
word(3)           # indexing, 3rd letter
##
is_a          = word == 'a'  # indicator array for letter 'a'
newword       = word;
newword(is_a) = 'o'          # indexed assignment
##
word.'                             # transpose
[char(word(1:end-1)-32) word(end)] # other operations

## Structures
# Structures are useful to store data with semantics. They can store any data 
# type.
#
# You can pre-allocate them, or create them on the fly.
#
S.one = 1; S.two = 2; S.three = 3
S = struct ('one', 1, 'two', 2, 'three', 3)
##
# We can create arrays of structures
S = [S; struct('one', 'I', 'two', 'II', 'three', 'III')]
S.two
S(1).three, S(2).three
##
# the fields can be accessed with variables
fieldname = 'one';
S(2).(fieldname)

## Cells
# Cells are like arrays, but they can store mixed types and shapes.
# The disadvantage is that they are slower than arrays, and in general functions
# do not work on them as with arrays.
#
x = [1.2, int8(1)]   # this demotes the double to int
y = {1.2, int8(1)}   # this doesn't
##
% x = [1; [1 2 3]]    # this fails
x = {1; [1 2 3]}      # this works
##
x = {1; [1 2 3]; '1 2 3'};
x([1 3])                  # a cell with the 1st and 3rd elements
x{2}                      # the 2nd element
##
x{2} = []                 # assigns to the 2nd element, does not remove
x(2) = []                 # removes the 2nd element

## Comma separated lists
# They are created when we do not enclose a comma separated list in brackets |[]|
# or braces |{}|. Inputs and outputs are comma separated lists.
#
max (1, 2)  # the arguments '1, 2' are a cs-list
##
[m, i] = max (1, 2) # trigger an error to see that outputs are cs-list as well

##
# Cells can be converted into comma separated lists by indexing with braces.
args = {1, 'first'};
args(:)            # cell
args{:}            # cs-list
1, 'first'         # same as above
##
# this is useful to pass arguments created within a program, or to simplify
# calls to functions with many input arguments.
x = sort (rand (1, 10))
find (x > 0.5, args{:})

## Copy on Write
# Lets illustrate CoW. We create a rather large variable and then pass it to
# a function that makes a copy of it, and monitors free memory to detect when
# it is actually copied.
#
clear all
A = rand (1024);  # 1024 x 1024 random matrix ~ 8 * 1e6 bytes = 8 MB
copyarg (A);

##
# This is the content of the file |copyarg.m|
#
# <include>copyarg.m</include>
#

## Flow control
# The most common blocks to control the flow of a program are if-elseif-else 
# for branching, and |for| and |while| for looping. 
#
# Execute different code depending on a condition using |if| blocks
#
x = 1; if (x > 0); disp ('positive!'); else; disp('negative'); endif
x = -1; if (x > 0); disp ('positive!'); else; disp('negative'); endif
##
x = 1; if (x > 0); disp ('positive!'); elseif (x < 0); disp('negative'); ...
  else; disp('zero!'); endif
x = -1; if (x > 0); disp ('positive!'); elseif (x < 0); disp('negative'); ...
  else; disp('zero!'); endif
x = 0; if (x > 0); disp ('positive!'); elseif (x < 0); disp('negative'); ...
  else; disp('zero!'); endif
##
# Loop over arrays or cells using |for| loops
x = linspace (-1, 1, 5);
for i = 1:length(x); disp (x(i)); endfor
##
for xi = x; disp (xi); endfor
##
for xi = x
  if ( abs(xi - fix (xi) ) < eps )
    printf ("%d is integer\n", xi);
  endif
endfor
##
# When looping over cells, the iterated variable is still a cell, index with
# |{1}| to get the content.
y = {1,'two',[1, 1], complex(0, 4)};
for c = y; {c c{1}}, endfor
##
# |while| blocks can be used when the looping is controlled by a condition
x = 10;
while x > 5; x -= 1; endwhile
x
##
x = 10;
while true; x -=1; if x <= 5; break; endif; endwhile # same as before
x
