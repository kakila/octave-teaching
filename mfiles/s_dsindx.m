## Copyright (C) 2015 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

N = 50;                     # Number of points
x = linspace (0, pi, N).';  # 1D Mesh
f = sin (x);                # Function evaluated on the mesh
h = x(2) - x(1);            # Spacing
d = [-1 1] / h;             # 2 points stencil
df = zeros (N,1);           # Initialization

for i = 2:N                 # Loop over all values except the first
  df(i) = d * f(i-1:i);
end

df(1) = -df(end);           # Boundary conditions

# Plot the result
plot (x, cos (x), x, df, '.');
legend ('Exact','Aprox');

# Mean squared error
printf ('MSE: %.2e\n', meansq (df - cos (x)) );
