## Copyright (C) 2017 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

# Basic use of accumarray
v       = exprnd (1e-1, [1e5,1]);
s       = randi (100, length (v), 1);
y       = accumarray (s, v, [100, 1]);

[cv xx] = hist (v, length (y), 1);
tmp     = max (v) * y / max (y);
cy      = hist (tmp, xx, 1);

h = bar (xx.', [cv.' cy.'], 'hist');
legend (h, {'data','accum'});
axis tight

