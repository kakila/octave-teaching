##
# Copyright (C) 2017 - Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## Using function handles to mask inputs
# Function handles can be used t progamatically change the
# signature of a function.
# This, among ther things, can be used for wrappers.

# the root mean squared error takes two data vectors
rmse = @(x,y) sqrt (meansq (x - y));
# We create data for x
t    = linspace (0, 1, 100) * pi;
data = sin (2 * t);
# We define the function we use as model for the data
# the function takes the parameters as input argument
model = @(p) cos (p(1) * t - pi * p(2));
# We Optimize the rmse to find the parameter values
cost = @(p) rmse (data, model(p));
p0   = [0; 0];
p    = fminsearch (cost, p0);
# Show results
printf ("Parameters: %.2f\t%.2f\n", p);
plot (t, data, 'o;data;', t, model (p), '-;model;');
axis tight
